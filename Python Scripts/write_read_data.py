im_path = "8.png"    
comPort = 'COM7'
baudrate = 921600

import numpy as np
import serial as ser
from PIL import Image
import matplotlib.pyplot as plt
import time

port = ser.Serial()
port.baudrate = baudrate
port.port = comPort

im_1 = Image.open(im_path)
image = ~np.array(im_1)
image = sum(image.T).T
plt.imshow(image)
DATA = bytes(image)

port.open()
port.write(DATA);
print(port.read(3))
port.close()

time.sleep(1)
port.open()
print("The uC recognized",ord(port.read()))
port.close()